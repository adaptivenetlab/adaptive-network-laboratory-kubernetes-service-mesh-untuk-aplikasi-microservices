**###LIST DOKUMEN###**

- [Ketentuan Dan Aturan Riset<br>](https://docs.google.com/document/d/1df5r8n8sMBciuN7-KYwg2My--GMNEHl7iXJA4vBtfsA/edit?usp=sharing)

- [Timeline Riset<br>](https://docs.google.com/spreadsheets/d/1RaTqJSlePa9e21E_OG0fp2UP2DQ8sCD9hd_gh3HVXY8/edit?usp=sharing)

- [Aturan Dokumentasi Riset<br>](https://docs.google.com/document/d/188HRr3ZXPqSR1YRvdk_ioOThYw1w_ThlWnarFDDbAJI/edit?usp=sharing)

- [Contoh Format Dokumentasi Riset + Screenshoot<br>](https://drive.google.com/file/d/1cTLl4_5T2UimscMfEPhCxYYJq7gIdvyb/view?usp=sharing)
